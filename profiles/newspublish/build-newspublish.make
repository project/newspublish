api = 2
core = 8.x
; Include the definition for how to build Drupal core directly, including patches:
includes[] = drupal-org-core.make
projects[newspublish][profile] = "newspublish"
projects[newspublish][download[type] = "git"
projects[newspublish][download[url] = "https://git.drupal.org/project/newspublish.git"
projects[newspublish][download][branch] = 8.x-1.x
